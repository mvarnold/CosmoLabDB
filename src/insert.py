"""
Database insert script for CosmoLab database

General plan is to have a function to handle each sheet in an excel file,
with one sql table per file type.

Once each sheet format is handled, the script will take a filename and directory path to insert a

05/29/2021
Michael Arnold

"""
import pandas as pd
import time
import sqlalchemy
from sqlalchemy import create_engine, inspect, event
from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy import Column, Integer, Float, String, Date

pd.options.display.width = 0
Base = declarative_base()

def db_engine(usr="mvarnold_admin", pwd="iOln04MJTjjE"):
    """ Database engine function: Use when not using the ORM (object relational mapping)

    :param usr:
    :param pwd:
    :return:
    """
    ssl_args = {'ssl': {'ca': '../login/webdb-cacert.pem'}}
    db_engine = create_engine(
        f"mysql://{usr}:{pwd}@webdb.uvm.edu/MVARNOLD_test",
        connect_args=ssl_args)
    return db_engine


def db_session(usr="mvarnold_admin", pwd="iOln04MJTjjE"):
    """ Generic database connection function

    I think it's fine to use my credentials
    since they are randomly generated just for this database,
    but I can make specific credentials if necessary.

    mvarnold_admin, with password: iOln04MJTjjE
    mvarnold_writer, with password: 9JSLPL7qZKZy
    mvarnold_reader, with password: SU561GrATgXj

    Args:
        usr: username
        pwd: password
    """
    ssl_args = {'ssl': {'ca': '../login/webdb-cacert.pem'}}

    # Todo: Change from test db to "MVARNOLD_cosmolabdb" when testing is finished

    db_engine = create_engine(
        f"mysql://{usr}:{pwd}@webdb.uvm.edu/MVARNOLD_test",
        connect_args=ssl_args)
    Session = sessionmaker(bind=db_engine)
    db = Session()
    return db


class RawAliquotICP(Base):
    """ Base class to define database schema for sheet 4"""
    # Notes: We'll want one of these classes for every sheet,

    __tablename__ = "raw_aliquot_ICP"

    batch_num = Column(Integer, primary_key=True)
    ID = Column(String(10), primary_key=True)
    Al_308 = Column(Float)
    Al_309 = Column(Float)
    Al_396 = Column(Float)
    Be_234 = Column(Float)
    Be_249 = Column(Float)
    Weigh_in_date = Column(Date)

    def __repr__(self):
        """ defines what text prints if you try to print this object"""
        return f"<Raw_aliquot_ICP(ID='{self.ID}'," \
               f"AL_308='{self.AL_308}'," \
               f"AL_309='{self.AL_309}'," \
               f"AL_396='{self.AL_396}'," \
               f"Be_234='{self.Be_234}'," \
               f"Be_234='{self.Be_234}'," \
               f"Be_249='{self.Be_249}'," \
               f"Batch='{self.batch}'," \
               f")>"


def load_extraction_data(fname):
    """ Load and clean relevant in situ extraction data from excel file

    Args:
        fname: xlsx filename to load

    """
    # Todo: need to confirm which sheets have relevant data
    sheet_indexes = [3]

    # column_names dictionary is indexed by sheet number
    column_names = {
        1: [
            "Sample ID",
            "Digest Bottle & Lid Mass (g)",
            "Sample Mass (g)",
            "Al Carrier Soln Mass (g)",
            "Be Carrier Soln Mass (g)",
            "Bottle, Lid & Soln Mass (g)",
            "2/4 ml Aliquot (g)",
            "4/8 ml Aliquot (g)",
            "2/4 ml Aliquot To ICP (g)",
            "4/8 ml Aliquot To ICP (g)",
            ],
        3: [
            ""
        ]
    }
    # header position for each sheet {sheet_index: header_row_index}
    header_dict = {
        1: 11,
        3: 7,
    }
    skip_rows = {
        1: 0,
        3: 0,
    }

    assert set(sheet_indexes).issubset(set(header_dict.keys())), \
        "Make sure header_dict has the same sheets as sheet_indexes"

    # Todo: confirm relevant columns and metadata to include
    extraction_dfs = {}
    for sheet in sheet_indexes:

        # sheet exceptions: if statements for cleaning each sheet
        if sheet == 1:
            pass  # skip until this sheet's function is implemented

        if sheet == 3:
            df = load_sheet_3(fname, sheet)
            print(df)

            i = input("Does this match the excel sheet? Y/n")
            # you could add something here to automate if you wanted...
            insert_sheet_3(df)

        extraction_dfs[sheet] = df

    return extraction_dfs


def load_sheet_3(fname, sheet):
    """ Loads and cleans sheet 3: PASTE aliquot ICP data

    Args:
        fname: filename for excel sheet
        sheet: integer sheet index

    Returns:
        df: pandas dataframe ready to insert
    """
    # ToDo: update df column names to whatever will be in the mySQL database

    df = pd.read_excel(fname,
                       sheet_name=sheet,
                       header=7,
                       skiprows=0,
                       )

    # drop row between index and first entry
    df.drop(df.index[0], inplace=True)

    # select only relevant data columns
    existing_columns = ['ID', 'Al 308.215', 'Al 309.271', 'Al 396.152', 'Be 234.861', 'Be 249.473']
    new_columns = {
        "ID": "ID",
        "Al 308.215": "Al_308",
        "Al 309.271": "Al_309",
        "Al 396.152": "Al_396",
        "Be 234.861": "Be_234",
        "Be 249.473": "Be_249",
    }

    df = df[existing_columns]
    df.rename(columns=new_columns, inplace=True)
    print(df)

    # add batch and date information           y
    batch = pd.read_excel(fname, sheet, header=0, usecols="J", squeeze=True).dropna()
    batch_num = batch.iloc[0]
    date = batch.iloc[1]
    df["Weigh_in_date"] = date
    df['batch_num'] = batch_num
    return df


def insert_sheet_3(df):
    """ Inserts a dataframe into the mysql database.

    Dataframe is already cleaned and column names match those defined in the associated column class: RawAliquotICP

    Args:
        df: Pandas Dataframe
    """
    start_time = time.time()
    session = db_session()
    mapper = inspect(RawAliquotICP)
    Base.metadata.create_all(db_engine())
    try:
        session.bulk_insert_mappings(mapper, df.to_dict(orient='records'))
        session.commit()
        session.close()
        print(f"Inserted in {time.time()-start_time:.2f}s")

    except sqlalchemy.exc.IntegrityError:
        print(f"Already inserted table 3 from Batch {df['batch_num'].values[0]}")
        pass

    return


def load_ams_data(fname):
    """ Load and clean ams data from excel file

    Args:
        fname: xlsx filename to load
    """
    sheet_indexes = []

    ams_df = pd.read_excel(fname,
                           sheet_name=sheet_indexes,
                           )

    return ams_df


def main():
    usr = "mvarnold_writer"
    pwd = "9JSLPL7qZKZy"
    fname = "../data/UVM_B429new.xls"
    extraction_dfs = load_extraction_data(fname)
    return extraction_dfs

    #insert_extraction_data(extraction_df)


if __name__ == "__main__":
    dfs = main()