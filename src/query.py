import pandas as pd
from sqlalchemy import create_engine

def db_engine(usr="mvarnold_admin", pwd="iOln04MJTjjE"):
    """ Database engine function: Use when not using the ORM (object relational mapping)

    :param usr:
    :param pwd:
    :return:
    """
    ssl_args = {'ssl': {'ca': '../login/webdb-cacert.pem'}}
    db_engine = create_engine(
        f"mysql://{usr}:{pwd}@webdb.uvm.edu/MVARNOLD_test",
        connect_args=ssl_args)
    return db_engine

engine = db_engine()
df = pd.read_sql("raw_aliquot_ICP", con=engine)

print(df)